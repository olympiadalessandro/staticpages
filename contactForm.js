/* contactForm.js */

/**
*Purpose is to send form fields containing user name, email, and message to recipient
*@author Olympia DAlessandro
*/


//When the page loads, run the init function
window.onload = init;

//Init function that finds the button on the page and handles the button event
function init() {
    
    //Find the button on the page and run it through the showImage function when clicked
	var button = document.getElementById("submit");
	button.onclick = displayMessage;
}

//Function that finds the user-entered text into the fields and displays the information into an alert
function displayMessage() {
    
    var userName = document.getElementById("userName").value;
    var userEmail = document.getElementById("userEmail").value;
    var userMessage = document.getElementById("userMessage").value;
    
    //Outputs the user's name, email, and message in an alert
    alert("Thank you " + userName + "!");
    
    console.log(userEmail + " says " + userMessage);
}